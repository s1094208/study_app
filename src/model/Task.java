package model;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

public class Task {
    private int schedule_id;
    private SimpleLongProperty startTime;
    private SimpleLongProperty endTime;
    private SimpleStringProperty description;

    public Task(int schedule_id, long startTime, long endTime, String description) {
        this.schedule_id = schedule_id;
        this.startTime = new SimpleLongProperty(startTime);
        this.endTime = new SimpleLongProperty(endTime);
        this.description = new SimpleStringProperty(description);
    }

    public Task() {

    }

    public void setStartTime(long startTime) {
        this.startTime = new SimpleLongProperty(startTime);
    }

    public long getStartTime() {
        return startTime.get();
    }

    public void setEndTime(long endTime) {
        this.endTime = new SimpleLongProperty(endTime);
    }

    public long getEndTime() {
        return endTime.get();
    }

    public void setDescription(String description) {
        this.description = new SimpleStringProperty(description);
    }

    public String getDescription() {
        return description.get();
    }

    public void setSchedule_id(int schedule_id) {
        this.schedule_id = schedule_id;
    }

    public int getSchedule_id() {
        return schedule_id;
    }

    public String toString() {
        return getTimestamp(getStartTime()) + " - " + getTimestamp(getEndTime()) + " : " + getDescription();
    }

    public String getTimes() {
        return getTimestamp(getStartTime()) + "-" + getEndTime();
    }

    public String getTimestamp(long time) {
        long minute = (time / (1000 * 60)) % 60;
        long hour = (time / (1000 * 60 * 60)) % 24;

        return String.format("%02d:%02d", hour, minute);
    }
}
