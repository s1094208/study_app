package model;

import java.util.ArrayList;

public class Schedule {
    private int id;
    private long weekDay;
    private String subject;
    private ArrayList<Task> taskList;
    private boolean atSchool;

    public Schedule() {

    }

    public Schedule(int id, long weekDay, String subject, boolean atSchool) {
        this.id = id;
        this.weekDay = weekDay;
        this.subject = subject;
        this.taskList = taskList;
        this.atSchool = atSchool;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setWeekDay(long weekDay) {
        this.weekDay = weekDay;
    }

    public long getWeekDay() {
        return weekDay;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public void setTaskList(ArrayList<Task> taskList) {
        this.taskList = taskList;
    }

    public ArrayList<Task> getTaskList() {
        return taskList;
    }

    public void setAtSchool(boolean atSchool) {
        this.atSchool = atSchool;
    }

    public boolean isAtSchool() {
        return atSchool;
    }
}
