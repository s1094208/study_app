package model;

import java.time.Duration;

public class VideoModel {
    private String videoLink;
    private long videoDuration;

    public VideoModel(String videoLink, long videoDuration){
        this.videoLink = videoLink;
        this.videoDuration = videoDuration;
    }

    public VideoModel() {

    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoDuration(long videoDuration) {
        this.videoDuration = videoDuration;
    }

    public long getVideoDuration() {
        return videoDuration;
    }
}
