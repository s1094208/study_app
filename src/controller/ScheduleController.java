package controller;

import model.Schedule;
import model.Task;

import java.util.ArrayList;
import java.util.Calendar;

public class ScheduleController {
    private ArrayList<Schedule> scheduleList;
    private Schedule currentSchedule;

    public ScheduleController(Schedule currentSchedule) {
        this.currentSchedule = currentSchedule;
    }

    public void setScheduleList(ArrayList<Schedule> scheduleList) {
        this.scheduleList = scheduleList;
    }

    public ArrayList<Schedule> getScheduleList() {
        return scheduleList;
    }

    public void setCurrentSchedule(Schedule currentSchedule) {
        this.currentSchedule = currentSchedule;
    }

    public Schedule getCurrentSchedule() {
        return currentSchedule;
    }

    public long getCurrentDay() {
        Calendar calendar = Calendar.getInstance();
        long CurrentDay = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        return CurrentDay;
    }

    public ArrayList<Task> getCurrentTasks() {
        return currentSchedule.getTaskList();
    }

    public Task getCurrentTask(long currentTime) {
        Task currentTask = new Task(currentSchedule.getId(), currentTime, currentTime+3600000, "Freedom");
        for (Task aTaskList : getCurrentTasks()) {
            if (currentTime > aTaskList.getStartTime() && currentTime < aTaskList.getEndTime()) {
                currentTask = aTaskList;
            }
        }


        return currentTask;
    }
}
