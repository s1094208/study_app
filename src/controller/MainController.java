package controller;

import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import view.RootView;
import view.WelcomeView;

public class MainController {

    private RootView rootView;
    private WelcomeController welcomeController;
    private WelcomeView welcomeView;
    private Stage primaryStage;

    public MainController(Stage primaryStage) {
        //Create the root view
        this.primaryStage = primaryStage;
        rootView = new RootView(primaryStage);

        //Create the welcomecontroller and view
        welcomeController = new WelcomeController(this);
        welcomeView = new WelcomeView(welcomeController);

        //Change the view to the welcomeview
        welcomeController.changePage(welcomeView);
        changePage(welcomeView);
    }

    public void changePage(Pane page) {
        rootView.changePage(page);
    }

    public Stage getStage() {
        return primaryStage;
    }
}
