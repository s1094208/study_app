package controller;

import model.VideoModel;

import java.util.ArrayList;

public class VideoController {
    private ArrayList<VideoModel> videoList;
    private int currentVideoId = 0;

    public VideoController(ArrayList<VideoModel> videoList) {
        this.videoList = videoList;
    }

    public void setVideoList(ArrayList<VideoModel> videoList) {
        this.videoList = videoList;
    }

    public ArrayList<VideoModel> getVideoList() {
        return videoList;
    }

    public long getTotalVideoDuration() {
        long totalDuration = 0;
        for (VideoModel aVideoList : videoList) {
            totalDuration += aVideoList.getVideoDuration();
        }
        return totalDuration;
    }

    public void addToVideoList(VideoModel videoModel) {
        this.videoList.add(videoModel);
        System.out.println(this.videoList);
    }

    public VideoModel getCurrentVideo() {
        return this.videoList.get(currentVideoId);
    }

    public boolean nextVideo() {
        return this.currentVideoId < this.videoList.size() - 1;

    }

    public void goToNext() {
        this.currentVideoId += 1;
    }

    public ArrayList<String> getSeenLinks() {
        ArrayList<String> seenLinks = new ArrayList<>();
        int count = 0;
        String videoLink = getCurrentVideo().getVideoLink();
        while (!videoLink.equals(videoList.get(count).getVideoLink()) && count < videoList.size()) {
            seenLinks.add(videoList.get(count).getVideoLink());
            count++;
        }
        return seenLinks;
    }
}
