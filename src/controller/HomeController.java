package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import model.Schedule;
import model.Task;
import model.VideoModel;
import persistence.ScheduleDAO;
import persistence.VideoDAO;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class HomeController extends ParentController {
    private ScheduleController scheduleController;
    private ScheduleDAO scheduleDAO = new ScheduleDAO();
    private ObservableList<Task> currentTasks;
    private VideoController videoController = new VideoController(new ArrayList<>());
    private VideoDAO videoDAO = new VideoDAO();

    private DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    public HomeController(MainController mainController) {
        super(mainController);

//        try {
//            scheduleDAO.addNewSchedule(createDummySchedule());
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }

        Schedule currentSchedule = null;
        try {

            //todo get correct weekday
            currentSchedule = scheduleDAO.getCurrentSchedule(3, true);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        scheduleController = new ScheduleController(currentSchedule);

        currentTasks =
                FXCollections.observableArrayList(scheduleController.getCurrentTasks());
        setVideos();
    }

    @Override
    public void changeParentView() {

    }

    public ObservableList<Task> getCurrentTasks() {
        return currentTasks;
    }

    private void setVideos() {
        try {
            videoController.setVideoList(videoDAO.getVideos());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addNewVideo(VideoModel video, String duration) {
        video.setVideoDuration(createTimeInSeconds(duration));
        System.out.println("Tijd om een nieuwe video toe te voegen");
        try {
            videoController.setVideoList(videoDAO.addVideo(video));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteSeenVideos() {
        ArrayList<String> seenLinks = videoController.getSeenLinks();
        System.out.println(seenLinks);
        try {
            ArrayList<VideoModel> newVideoList = videoDAO.deleteVideos(seenLinks);
            videoController.setVideoList(newVideoList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<VideoModel> getVideos() {
        return videoController.getVideoList();
    }

    public VideoModel getCurrentVideo() {
        return videoController.getCurrentVideo();
    }

    public boolean updateVideo() {
        videoController.goToNext();
        return videoController.nextVideo();
    }

    private long createTimeInSeconds(String time) {
        String[] data = time.split(":");

        int hours  = Integer.parseInt(data[0]);
        int minutes = Integer.parseInt(data[1]);
        int seconds = Integer.parseInt(data[2]);

        int time2 = seconds + 60 * minutes + 3600 * hours;
        return TimeUnit.MILLISECONDS.convert(time2, TimeUnit.SECONDS);
    }

    public boolean checkForPauze() {
        Calendar cal = Calendar.getInstance();
        long CurrentTime = createTimeInSeconds(dateFormat.format(cal.getTime()));
        Task currentTask = scheduleController.getCurrentTask(CurrentTime);
        return currentTask.getDescription().equals("Pauze") || currentTask.getDescription().equals("Freedom");
    }

    public long getCurrentTime() {
        Calendar cal = Calendar.getInstance();
        return createTimeInSeconds(dateFormat.format(cal.getTime()));
    }

    public Task getCurrentTask() {

        return scheduleController.getCurrentTask(getCurrentTime());
    }

    private Schedule createDummySchedule() {
        ArrayList<Task> taskList = new ArrayList<>();
        Task firstTask = new Task();
        firstTask.setStartTime(createTimeInSeconds("08:30:00"));
        firstTask.setEndTime(createTimeInSeconds("10:30:00"));
        firstTask.setDescription("Les");
        taskList.add(firstTask);
        Task secondTask = new Task();
        secondTask.setStartTime(createTimeInSeconds("10:30:10"));
        secondTask.setEndTime(createTimeInSeconds("10:45:00"));
        secondTask.setDescription("Pauze");
        taskList.add(secondTask);
        Task thirdTask = new Task();
        thirdTask.setStartTime(createTimeInSeconds("10:45:10"));
        thirdTask.setEndTime(createTimeInSeconds("11:45:00"));
        thirdTask.setDescription("Info les leren");
        taskList.add(thirdTask);
        Task fourthTask = new Task();
        fourthTask.setStartTime(createTimeInSeconds("11:45:10"));
        fourthTask.setEndTime(createTimeInSeconds("12:00:00"));
        fourthTask.setDescription("Pauze");
        taskList.add(fourthTask);
        Task fifthTask = new Task();
        fifthTask.setStartTime(createTimeInSeconds("12:00:10"));
        fifthTask.setEndTime(createTimeInSeconds("13:00:00"));
        fifthTask.setDescription("Vorige les herhalen");
        taskList.add(fifthTask);

        Schedule schedule = new Schedule();
        schedule.setWeekDay(3);
        schedule.setSubject("iitorg");
        schedule.setTaskList(taskList);
        schedule.setAtSchool(true);

       return schedule;
    }

    public Stage getStage() {
        return mainController.getStage();
    }
}
