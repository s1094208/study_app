package controller;

import javafx.scene.layout.Pane;
import view.HomeView;

public class WelcomeController extends ParentController {

    public WelcomeController(MainController mainController) {
        super(mainController);



    }

    @Override
    public void changeParentView() {

    }

    public void goToHome() {
        HomeController homeController = new HomeController(mainController);
        HomeView homeView = new HomeView(homeController);
        mainController.changePage(homeView);
    }
}
