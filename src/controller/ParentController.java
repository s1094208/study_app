package controller;

import javafx.scene.layout.Pane;

public abstract class ParentController {
    protected MainController mainController;
    protected Pane page;

    public ParentController(MainController mainController) {
        this.mainController = mainController;
    }


    public abstract void changeParentView();


    public void changePage(Pane page) {
        this.page = page;
    }
}
