package persistence;

import model.VideoModel;
import util.DatabaseConnector;

import java.sql.*;
import java.util.ArrayList;

public class VideoDAO {

    private final static String getVideosQuery = "SELECT * FROM video;";
    private final static String addVideoQuery = "INSERT INTO video (videoLink, videoDuration)" +
            "VALUES (?, ?);";
    private final static String deleteVideosQuery = "DELETE FROM video WHERE videoLink" +
            " = ANY (?);";

    public VideoDAO() {

    }

    public ArrayList<VideoModel> getVideos() throws SQLException {
        ArrayList<VideoModel> videoList = new ArrayList<>();

        Connection databaseConnection = null;
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            databaseConnection = DatabaseConnector.getConnection();
            stmt = databaseConnection.prepareStatement(getVideosQuery);
            resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                VideoModel video = new VideoModel();

                video.setVideoLink(resultSet.getString("videoLink"));
                video.setVideoDuration(resultSet.getLong("videoDuration"));

                videoList.add(video);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            resultSet.close();
            stmt.close();
            DatabaseConnector.closeConnection(databaseConnection);
        }

        return videoList;
    }

    public ArrayList<VideoModel> addVideo(VideoModel videoModel) throws SQLException {
        System.out.println("We gaan nu toevoegen");
        Connection databaseConnection = null;
        PreparedStatement stmt = null;
        try {
            databaseConnection = DatabaseConnector.getConnection();
            stmt = databaseConnection.prepareStatement(addVideoQuery);
            stmt.setString(1, videoModel.getVideoLink());
            stmt.setLong(2, videoModel.getVideoDuration());
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            stmt.close();
            DatabaseConnector.closeConnection(databaseConnection);
        }

        return getVideos();
    }

    public ArrayList<VideoModel> deleteVideos(ArrayList<String> videoLinkList) throws SQLException {
        System.out.println(videoLinkList);
        Connection databaseConnection = null;
        PreparedStatement stmt = null;

        try {
            databaseConnection = DatabaseConnector.getConnection();
            stmt = databaseConnection.prepareStatement(deleteVideosQuery);
            stmt.setArray(1, databaseConnection.createArrayOf("varchar", videoLinkList.toArray()));
            System.out.println(stmt);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            stmt.close();
            DatabaseConnector.closeConnection(databaseConnection);
        }

        return getVideos();
    }
}
