package persistence;

import model.Schedule;
import model.Task;
import util.DatabaseConnector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ScheduleDAO {

    private final static String getSchedulesQuery = "SELECT * FROM schedule;";
    private final static String getCurrentScheduleQuery = "SELECT schedule.*, task.starttime, task.endtime, " +
            "task.description FROM schedule INNER JOIN task ON schedule.id = task.schedule_id " +
            "WHERE schedule.weekday = ? AND schedule.atschool = ?;";
    private final static String createNewScheduleQuery = "INSERT INTO schedule (weekday, atschool, subject) " +
            "VALUES (?, ?, ?) RETURNING id;";

    private TaskDAO taskDAO = new TaskDAO();

    public ScheduleDAO() {

    }

    public ArrayList<Schedule> getSchedules() throws SQLException {
        ArrayList<Schedule> scheduleList = new ArrayList<>();

        Connection databaseConnection = null;
        PreparedStatement stmt = null;
        ResultSet resultSet = null;

        try {
            databaseConnection = DatabaseConnector.getConnection();
            stmt = databaseConnection.prepareStatement(getSchedulesQuery);
            resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                Schedule schedule = new Schedule();

                schedule.setWeekDay(resultSet.getLong("weekDay"));
                schedule.setSubject(resultSet.getString("subject"));
                schedule.setAtSchool(resultSet.getBoolean("atSchool"));

                scheduleList.add(schedule);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            resultSet.close();
            stmt.close();
            DatabaseConnector.closeConnection(databaseConnection);
        }

        return scheduleList;
    }

    public Schedule getCurrentSchedule(int weekDay, boolean atSchool) throws SQLException {
        Schedule schedule = new Schedule();
        ArrayList<Task> currentTaskList = new ArrayList<>();

        Connection databaseConnection = null;
        PreparedStatement stmt = null;
        ResultSet resultSet = null;

        try {
            databaseConnection = DatabaseConnector.getConnection();
            stmt = databaseConnection.prepareStatement(getCurrentScheduleQuery);
            stmt.setInt(1, weekDay);
            stmt.setBoolean(2, atSchool);
            resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                schedule.setWeekDay(resultSet.getLong("weekday"));
                schedule.setSubject(resultSet.getString("subject"));
                schedule.setAtSchool(resultSet.getBoolean("atSchool"));

                Task task = new Task();
                task.setSchedule_id(resultSet.getInt("id"));
                task.setStartTime(resultSet.getLong("starttime"));
                task.setEndTime(resultSet.getLong("endtime"));
                task.setDescription(resultSet.getString("description"));
                currentTaskList.add(task);
            }
            schedule.setTaskList(currentTaskList);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            resultSet.close();
            stmt.close();
            DatabaseConnector.closeConnection(databaseConnection);
        }

        return schedule;
    }

    public void addNewSchedule(Schedule newSchedule) throws SQLException {
        int scheduleId = 0;

        Connection databaseConnection = null;
        PreparedStatement stmt = null;
        ResultSet resultSet = null;

        try {
            databaseConnection = DatabaseConnector.getConnection();
            stmt = databaseConnection.prepareStatement(createNewScheduleQuery);
            stmt.setLong(1, newSchedule.getWeekDay());
            stmt.setBoolean(2, newSchedule.isAtSchool());
            stmt.setString(3, newSchedule.getSubject());
            resultSet = stmt.executeQuery();
            resultSet.next();
            scheduleId = resultSet.getInt(1);
            System.out.println(scheduleId);
            taskDAO.addNewTasks(scheduleId, newSchedule.getTaskList());
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            resultSet.close();
            stmt.close();
            DatabaseConnector.closeConnection(databaseConnection);
        }

    }
}
