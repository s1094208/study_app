package persistence;

import model.Task;
import util.DatabaseConnector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TaskDAO {

    private final static String getTasksQuery = "SELECT * FROM task;";
    private final static String createNewTasksQuery = "INSERT INTO task (starttime, schedule_id, endtime, " +
            "description) VALUES (?, ?, ?, ?);";

    public TaskDAO() {

    }

    public ArrayList<Task> getTasks() throws SQLException {
        ArrayList<Task> taskList = new ArrayList<>();

        Connection databaseConnection = null;
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            databaseConnection = DatabaseConnector.getConnection();
            stmt = databaseConnection.prepareStatement(getTasksQuery);
            resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                Task task = new Task();

                task.setSchedule_id(resultSet.getInt("schedule_id"));
                task.setStartTime(resultSet.getLong("startTime"));
                task.setEndTime(resultSet.getLong("endTime"));
                task.setDescription(resultSet.getString("description"));

                taskList.add(task);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            resultSet.close();
            stmt.close();
            DatabaseConnector.closeConnection(databaseConnection);
        }

        return taskList;
    }

    public void addNewTasks(int schedule_id, ArrayList<Task> taskList) throws SQLException {
        Connection databaseConnection = null;
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            databaseConnection = DatabaseConnector.getConnection();
            stmt = databaseConnection.prepareStatement(createNewTasksQuery);
            for (int i = 0; i < taskList.size(); i++) {
                stmt.setLong(1, taskList.get(i).getStartTime());
                stmt.setInt(2, schedule_id);
                stmt.setLong(3, taskList.get(i).getEndTime());
                stmt.setString(4, taskList.get(i).getDescription());
                stmt.addBatch();
            }
            stmt.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            stmt.close();
            DatabaseConnector.closeConnection(databaseConnection);
        }
    }
}
