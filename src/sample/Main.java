package sample;

import controller.MainController;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    private MainController mainController;

    @Override
    public void start(Stage primaryStage) {

        primaryStage.setWidth(1500);
        primaryStage.setHeight(800);
        primaryStage.getIcons().add(new Image("/notebook.png"));
        primaryStage.setTitle("Hello World");

        mainController = new MainController(primaryStage);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
