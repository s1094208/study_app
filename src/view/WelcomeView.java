package view;

import controller.WelcomeController;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.util.Duration;

public class WelcomeView extends StackPane {

    private ImageView imageView = new ImageView();
    private ImageView textImageView = new ImageView();

    private Image bookImage = new Image(getClass().getResource("/talking.png").toExternalForm());
    private Image textImage = new Image(getClass().getResource("/welkom.png").toExternalForm());

    HBox speechBox = new HBox();

    private WelcomeController welcomeController;

    public WelcomeView(WelcomeController welcomeController) {

        this.welcomeController = welcomeController;

        createImage(imageView, bookImage);
        createImage(textImageView, textImage);

        textImageView.setFitHeight(10);
        textImageView.setFitWidth(10);
        textImageView.setVisible(false);

        Timeline initiateTimeline = initiateTimeline();
        initiateTimeline.play();

        Timeline textTimeline = textTimeline();
        initiateTimeline.setOnFinished(e -> {
            speechBox.getChildren().add(textImageView);
            textImageView.setVisible(true);
            textTimeline.play();
        });
        textTimeline.setOnFinished(e -> {
            speechBox.getChildren().remove(textImageView);
            welcomeController.goToHome();
        });


        speechBox.setAlignment(Pos.CENTER);
        speechBox.getChildren().addAll(imageView);

        BorderPane testPane = new BorderPane();

        testPane.setCenter(speechBox);

        this.getChildren().addAll(testPane);

    }

    private void createImage(ImageView imageView, Image image) {
        imageView.setImage(image);
        imageView.setPreserveRatio(true);
    }

    private Timeline initiateTimeline() {
        KeyFrame startFrame = new KeyFrame(Duration.ZERO,
                new KeyValue(imageView.fitHeightProperty(), 100),
                new KeyValue(imageView.fitWidthProperty(), 100));
        KeyFrame endFrame = new KeyFrame(Duration.millis(500),
                new KeyValue(imageView.fitHeightProperty(), 250),
                new KeyValue(imageView.fitWidthProperty(), 250));
        Timeline timeline = new Timeline(startFrame, endFrame);

        return timeline;
    }

    private Timeline textTimeline() {
        KeyFrame startFrame = new KeyFrame(Duration.ZERO,
                new KeyValue(textImageView.fitHeightProperty(), 10),
                new KeyValue(textImageView.fitWidthProperty(), 10));
        KeyFrame seeFrame = new KeyFrame(Duration.millis(400),
                new KeyValue(textImageView.fitHeightProperty(), 200),
                new KeyValue(textImageView.fitWidthProperty(), 200));
        KeyFrame stayFrame = new KeyFrame(Duration.millis(1100),
                new KeyValue(textImageView.fitHeightProperty(), 200),
                new KeyValue(textImageView.fitWidthProperty(), 200));
        KeyFrame endFrame = new KeyFrame(Duration.millis(1400),
                new KeyValue(textImageView.fitHeightProperty(), 10),
                new KeyValue(textImageView.fitWidthProperty(), 10));
        Timeline timeline = new Timeline();
        timeline.getKeyFrames().addAll(startFrame, seeFrame, stayFrame, endFrame);

        return timeline;
    }
}
