package view;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class RootView extends StackPane {

    public RootView(Stage stage) {
        Scene scene = new Scene(this, 1024, 768);
        stage.setScene(scene);
    }

    public void changePage(Pane page) {
        this.getChildren().clear();
        this.getChildren().add(page);
    }
}
