package view;

import controller.HomeController;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Popup;
import javafx.util.Duration;
import model.Task;
import model.VideoModel;

public class HomeView extends StackPane {

    private HomeController homeController;

    private ImageView textView = new ImageView();

    private BorderPane fullHomePane = new BorderPane();

    private HBox talkingBox = new HBox();
    private HBox videoControls = new HBox();
    private HBox optionsBox = new HBox();

    private Button videoButton = new Button();
    private Button nextButton = new Button();

    private WebView youtubePage = new WebView();

    private Timeline breakTimeline = new Timeline();
    private Timeline studyTimeline = new Timeline();

    private VBox addVideoForm = new VBox();

    public HomeView(HomeController homeController) {
        this.homeController = homeController;

        ImageView imageView = new ImageView();
        Image bookImage = new Image(getClass().getResource("/homeBook.png").toExternalForm());
        createImage(imageView, bookImage);
        imageView.setOnMouseClicked(e -> showOptions());
        Image textImage = new Image(getClass().getResource("/what.png").toExternalForm());
        createImage(textView, textImage);
        talkingBox.setAlignment(Pos.CENTER);
        talkingBox.getChildren().add(imageView);
        createTable();
        createStudyTimeline();
        createPauzeTimeline();

        videoButton.setText("Kijk video's");
        videoButton.setOnMouseClicked(e -> {
            talkingBox.getChildren().remove(textView);
            this.getChildren().remove(talkingBox);
            showVideo();
        });
        optionsBox.getChildren().add(videoButton);

        Button addVideoButton = new Button();
        addVideoButton.setText("Video toevoegen");
        addVideoButton.setOnMouseClicked(e -> {
            System.out.println("Geklikt op de toevoegknop");
            addNewVideo();
        });
        optionsBox.getChildren().add(addVideoButton);

        videoControls.setMaxHeight(480);
        videoControls.getChildren().addAll(youtubePage, nextButton);

        this.getChildren().addAll(talkingBox, fullHomePane);
        fullHomePane.setPickOnBounds(false);
    }

    private void createImage(ImageView imageView, Image image) {
        imageView.setImage(image);
        imageView.setPreserveRatio(true);
        imageView.setFitWidth(150);
        imageView.setFitHeight(150);
    }

    private void addNewVideo() {
        Popup addVideoPopup = new Popup();
        TextField videoUrl = new TextField();
        videoUrl.setText("Enter video url");
        videoUrl.setMaxWidth(400);
        addVideoForm.getChildren().add(videoUrl);

        TextField durationField = new TextField();
        durationField.setText("00:00:30");
        durationField.setMaxWidth(250);
        addVideoForm.getChildren().add(durationField);

        Button addVideoButton = new Button();
        addVideoButton.setText("Add video");
        addVideoButton.setOnAction(e -> {
            VideoModel newVideo = new VideoModel();
            newVideo.setVideoLink(videoUrl.getText());
            homeController.addNewVideo(newVideo, durationField.getText());
            addVideoPopup.hide();
        });
        addVideoForm.getChildren().add(addVideoButton);
        addVideoPopup.getContent().add(addVideoForm);

        addVideoPopup.show(homeController.getStage());
    }

    private void showOptions() {
        if (talkingBox.getChildren().contains(textView)) {
            talkingBox.getChildren().remove(textView);
            fullHomePane.getChildren().remove(optionsBox);
        } else {
                talkingBox.getChildren().add(textView);
            if (homeController.checkForPauze()) {
                fullHomePane.setCenter(optionsBox);
                optionsBox.setAlignment(Pos.CENTER);
                BorderPane.setMargin(optionsBox, new Insets(220,350,0,0));
            }
        }
    }

    private void createTable() {
        TableView<Task> scheduleTable = new TableView();

        TableColumn<Task, String> startCol = new TableColumn("StartTime");
        startCol.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTimestamp(cellData.getValue().getStartTime())));
        TableColumn<Task, String> endCol = new TableColumn("EndTime");
        endCol.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTimestamp(cellData.getValue().getEndTime())));
        TableColumn descCol = new TableColumn("Description");
        descCol.setCellValueFactory(
                new PropertyValueFactory<>("description"));

        scheduleTable.setItems(homeController.getCurrentTasks());
        scheduleTable.getColumns().addAll(startCol, endCol, descCol);
        scheduleTable.setMaxHeight(300);
        fullHomePane.setLeft(scheduleTable);
    }

    private void updateVideo() {
        if (!homeController.updateVideo()) {
            videoControls.getChildren().remove(nextButton);
        }
        showVideo();
    }

    private void showVideo() {
        VideoModel currentVideo = homeController.getCurrentVideo();
        youtubePage.getEngine().load(
                "https://www.youtube.com/embed/" + currentVideo.getVideoLink()
        );
        youtubePage.setPrefSize(854, 480);
        if(!fullHomePane.getChildren().contains(videoControls)) {
            fullHomePane.setCenter(videoControls);

            nextButton.setText("Next Video");
            nextButton.setOnAction((ActionEvent e) -> updateVideo());
        }
    }

    private void createPauzeTimeline() {
        videoButton.setOnAction((ActionEvent e) -> {
            breakTimeline = new Timeline(new KeyFrame(Duration.seconds(1), ev -> {
                if (!homeController.getCurrentTask().getDescription().equals("Pauze") && !homeController.getCurrentTask().getDescription().equals("Freedom")) {
                    stopCheckingTime();
                    this.getChildren().add(talkingBox);
                    fullHomePane.getChildren().remove(videoControls);
                    studyTimeline.play();
                } else if (homeController.getCurrentTask().getEndTime() - homeController.getCurrentTime() == 60000) {
                    System.out.println("Nog een minuut te gaan!");
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    Image image = new Image(getClass().getResource("/time1min.png").toExternalForm());
                    ImageView imageView = new ImageView(image);
                    alert.setGraphic(imageView);
                    alert.show();
                }
            }));
            breakTimeline.setCycleCount(Timeline.INDEFINITE);
            breakTimeline.play();
            System.out.println("hoi");

        });
    }

    private void createStudyTimeline() {
        studyTimeline = new Timeline(new KeyFrame(Duration.seconds(1), ev -> {
            if (!homeController.getCurrentTask().getDescription().equals("Pauze") && !homeController.getCurrentTask().getDescription().equals("Freedom")) {
                if (homeController.getCurrentTask().getEndTime() - homeController.getCurrentTime() == 120000) {
                    System.out.println("Nog 2 minuten te gaan!");
                    Alert alert2 = new Alert(Alert.AlertType.WARNING);
                    Image image2 = new Image(getClass().getResource("/work2min.png").toExternalForm());
                    ImageView imageView2 = new ImageView(image2);
                    alert2.setGraphic(imageView2);
                    alert2.show();
                }
            } else {
                breakTimeline.play();
                stopStudying();
            }
        }));
        studyTimeline.setCycleCount(Timeline.INDEFINITE);
        studyTimeline.play();
    }

    private void stopCheckingTime() {
        System.out.println("gestopt");
        Alert deleteAlert = new Alert(Alert.AlertType.CONFIRMATION);
        Image deleteImage = new Image(getClass().getResource("/deletevideos.png").toExternalForm());
        ImageView deleteImageView = new ImageView(deleteImage);
        deleteAlert.setGraphic(deleteImageView);

        deleteAlert.setOnHidden(e -> {
            ButtonType result = deleteAlert.getResult();
            if (result == ButtonType.OK) {
                homeController.deleteSeenVideos();
            }
        });
        deleteAlert.show();
        breakTimeline.stop();
    }

    private void stopStudying() {
        studyTimeline.stop();
    }
}
