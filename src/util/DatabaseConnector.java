package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnector {

    private static final String DB_CONNECTION = "jdbc:postgresql://localhost:5432/study-app";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "610goofY";

    private DatabaseConnector() {

    }

    public static Connection getConnection() throws SQLException {
        Connection databaseConnection = null;
        databaseConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);

        System.out.println("Database connection opened");
        return databaseConnection;
    }

    public static void closeConnection(Connection connection) throws SQLException {
        connection.close();
        System.out.println("Database connection closed");
    }
}
